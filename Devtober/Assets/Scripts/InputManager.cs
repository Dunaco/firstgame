﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    public Motor motor;
    public PlayerData playerData;


    public void Awake()
    {
        
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        // Actions Based on Key Pressed by User
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            // Move Forward
            motor.Move(playerData.movementSpeed);
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            //Turn Left
            motor.Rotate(-playerData.turnSpeed);
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            // Go Backwards
            motor.Move(-playerData.movementSpeed);
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            // Turn Right
            motor.Rotate(playerData.turnSpeed);
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            //TODO: Bring up Pause Menu
        }
        if (Input.GetKey(KeyCode.Space))
        {
            //TODO: Perform Action
        }
    }
}

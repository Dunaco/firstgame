﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour
{

    private CharacterController characterController;

    private Transform tf;

    public void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    // Start is called before the first frame update
    void Start()
    {
        characterController = gameObject.GetComponent<CharacterController>();
    }

    public void Move(float moveSpeed)
    {
        Vector3 speedVector = tf.forward;

        characterController.SimpleMove(moveSpeed * speedVector);
    }

    public void Rotate(float turnSpeed)
    {
        Vector3 rotateVector = Vector3.up;

        tf.Rotate(rotateVector * turnSpeed, Space.Self);
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
